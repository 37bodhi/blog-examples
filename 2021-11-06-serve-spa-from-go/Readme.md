To run the example code:

1. Make sure you have `yarn` and `go` installed
2. Build the frontend assets

    ```bash
    cd frontend
    yarn && yarn build
    ```

3. Run the Go server:

    ```bash
    cd ..
    go run .
    ```

You can then access the app at <http://localhost:8080>.
