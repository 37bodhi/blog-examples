package reddit

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/hackandsla.sh/blog-examples/startup/jsondb"
)

type Config struct {
	BaseURL   string
	UserAgent string
	Subreddit string
}

type RedditPoller struct {
	client *http.Client
	config *Config
	db     *jsondb.JSONDB
}

func New(db *jsondb.JSONDB) *RedditPoller {
	return &RedditPoller{
		client: &http.Client{},
		config: &Config{
			BaseURL:   "https://www.reddit.com",
			UserAgent: "golang:sh.hackandsla.service-example:0.0.1 (by /u/terrabitz)",
			Subreddit: "golang",
		},
		db: db,
	}
}

func (r *RedditPoller) Start(ctx context.Context) {
	log.Println("Reddit watcher service starting")

	ticker := time.NewTicker(60 * time.Second)

	go func() {
		defer ticker.Stop()

		for {
			select {
			case <-ticker.C:
				log.Printf("Retrieving posts from subreddit '%s'\n", r.config.Subreddit)
				posts, err := r.GetRecentPosts(ctx)
				if err != nil {
					log.Printf("Error while getting reddit posts: %v\n", err)
				}

				r.db.SavePosts(posts)
			case <-ctx.Done():
				log.Println("Reddit watcher service stopping")
				return
			}
		}
	}()
}

func (r *RedditPoller) GetRecentPosts(ctx context.Context) ([]jsondb.Post, error) {
	url := fmt.Sprintf("%s/r/%s/.json?limit=100&sort=new", r.config.BaseURL, r.config.Subreddit)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("User-Agent", r.config.UserAgent)

	res, err := r.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("could not retrieve reddit posts: %w", err)
	}

	var sr SubResponse
	if err := json.Unmarshal(body, &sr); err != nil {
		return nil, err
	}

	var ret []jsondb.Post
	for _, child := range sr.Data.Children {
		ret = append(ret, child.Data)
	}

	return ret, nil
}

type SubResponse struct {
	Kind string `json:"kind"`
	Data struct {
		Modhash  string `json:"modhash"`
		Dist     int    `json:"dist"`
		Children []struct {
			Kind string      `json:"kind"`
			Data jsondb.Post `json:"data"`
		} `json:"children"`
		After  string `json:"after"`
		Before string `json:"before"`
	} `json:"data"`
}
