package jsondb

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sort"
	"sync"
)

type JSONDB struct {
	mu    sync.Mutex
	posts map[string]Post

	filePath string
}

func New(filePath string) *JSONDB {
	return &JSONDB{
		posts:    make(map[string]Post),
		filePath: filePath,
	}
}

func (db *JSONDB) Start() error {
	log.Println("JSON DB service starting")

	if !fileExists(db.filePath) {
		if err := createJSONFile(db.filePath); err != nil {
			return err
		}
	}

	data, err := readJSONFile(db.filePath)
	if err != nil {
		return err
	}

	db.setPosts(data)

	return nil
}

func (db *JSONDB) Stop() {
	log.Println("JSON DB service stopping")

	if err := db.saveToJSONFile(); err != nil {
		log.Printf("error while saving file '%s': %v", db.filePath, err)
	}
}

func (db *JSONDB) SavePosts(posts []Post) {
	db.mu.Lock()
	defer db.mu.Unlock()

	for _, post := range posts {
		if _, ok := db.posts[post.ID]; !ok {
			db.posts[post.ID] = post
		}
	}
}

func (db *JSONDB) GetPosts() []Post {
	db.mu.Lock()
	defer db.mu.Unlock()

	var posts []Post
	for _, v := range db.posts {
		posts = append(posts, v)
	}

	byCreatedAt := func(i, j int) bool {
		return posts[i].Created < posts[j].Created
	}

	sort.Slice(posts, byCreatedAt)

	return posts
}

func (db *JSONDB) setPosts(posts map[string]Post) {
	db.mu.Lock()
	db.posts = posts
	db.mu.Unlock()
}

func (db *JSONDB) saveToJSONFile() error {
	db.mu.Lock()
	defer db.mu.Unlock()

	fileContents, err := json.MarshalIndent(db.posts, "", " ")
	if err != nil {
		return fmt.Errorf("error while marshaling JSON: %w", err)
	}

	if err := os.WriteFile(db.filePath, fileContents, 0644); err != nil {
		return fmt.Errorf("error while writing to '%s': %w", db.filePath, err)
	}

	return nil
}

func readJSONFile(filePath string) (map[string]Post, error) {
	fileContents, err := os.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("error while reading archive '%s': %w", filePath, err)
	}

	ret := make(map[string]Post)
	if err := json.Unmarshal(fileContents, &ret); err != nil {
		return nil, fmt.Errorf("error while unmarshaling archive '%s': %w", filePath, err)
	}

	return ret, nil
}

func createJSONFile(filePath string) error {
	if err := os.WriteFile(filePath, []byte("{}"), 0644); err != nil {
		return fmt.Errorf("error while creating '%s': %w", filePath, err)
	}

	return nil
}
func fileExists(filePath string) bool {
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return false
	}

	return true
}

type Post struct {
	Title     string  `json:"title,omitempty"`
	Domain    string  `json:"domain,omitempty"`
	URL       string  `json:"url,omitempty"`
	Permalink string  `json:"permalink,omitempty"`
	ID        string  `json:"id,omitempty"`
	PostText  string  `json:"selftext,omitempty"`
	Created   float32 `json:"created,omitempty"`
	NSFW      bool    `json:"over_18,omitempty"`
}
