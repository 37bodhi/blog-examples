package httpserver

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/hackandsla.sh/blog-examples/startup/jsondb"
)

type HTTPServer struct {
	server *http.Server
	db     *jsondb.JSONDB
}

func New(db *jsondb.JSONDB) *HTTPServer {
	svc := &HTTPServer{
		db: db,
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/posts", svc.handleGetPosts())

	svc.server = &http.Server{
		Addr:    ":8080",
		Handler: mux,
	}

	return svc
}

func (h *HTTPServer) Start(errChan chan<- error) {
	log.Println("HTTP server starting")

	go func() {
		if err := h.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			errChan <- fmt.Errorf("error during HTTP server start: %w", err)
		}
	}()
}

func (h *HTTPServer) Stop() {
	log.Println("HTTP server stopping")

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*1)
	defer cancel()

	h.server.SetKeepAlivesEnabled(false)

	if err := h.server.Shutdown(ctx); err != nil {
		log.Printf("error while stopping HTTP server: %v", err)
	}
}

func (h *HTTPServer) handleGetPosts() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		posts := h.db.GetPosts()

		j, err := json.MarshalIndent(&posts, "", "  ")
		if err != nil {
			http.Error(w, "error marshaling JSON", http.StatusInternalServerError)
			return
		}

		if _, err := io.Copy(w, bytes.NewReader(j)); err != nil {
			http.Error(w, "error while writing response", http.StatusInternalServerError)
			return
		}
	}
}
