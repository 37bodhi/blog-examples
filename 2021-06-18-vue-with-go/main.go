package main

import (
	"bytes"
	"embed"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log"
	"math/rand"
	"net/http"

	"github.com/rs/cors"
)

//go:embed frontend/dist
var frontend embed.FS

func main() {
	// option1()
	// option2()
	Option3()
}

// Option1 uses the Vue dev server to serve the development frontend assets, and
// the Go server running on a different port for the backend. This requires
// CORS, since the frontend and backend run on different origin URLs.
func Option1() {
	var port int
	flag.IntVar(&port, "port", 8080, "The port to listen on")
	flag.Parse()

	// Add CORS middleware to allow access from the Vue development server
	// running on http://localhost:8080
	corsMiddleware := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:8080"},
	})
	http.Handle("/api/v1/law", corsMiddleware.Handler(http.HandlerFunc(getRandomLaw)))

	stripped, err := fs.Sub(frontend, "frontend/dist")
	if err != nil {
		log.Fatalln(err)
	}

	http.Handle("/", http.FileServer(http.FS(stripped)))

	log.Fatalln(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}

// Option2 uses the Vue dev server to serve the development frontend assets, and
// the Go server running on a different port for the backend. It also uses a
// proxy configuration in the Vue dev serve to proxy API requests to the
// backend, meaning no CORS configuration is required.
func Option2() {
	var port int
	flag.IntVar(&port, "port", 8080, "The port to listen on")
	flag.Parse()

	http.Handle("/api/v1/law", http.HandlerFunc(getRandomLaw))

	stripped, err := fs.Sub(frontend, "frontend/dist")
	if err != nil {
		log.Fatalln(err)
	}

	http.Handle("/", http.FileServer(http.FS(stripped)))

	log.Fatalln(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}

// Option3 uses our Go server to serve the development frontend assets. This
// assumes that the frontend assets will be continuously rebuilt via `yarn
// watch`, and that the implementation of getFrontendAssets will be filled in at
// build-time.
func Option3() {
	http.Handle("/api/v1/law", http.HandlerFunc(getRandomLaw))

	// The implementation of this function will be filled in by either the code
	// in fs_dev.go or fs_prod.go, depending on whether the `prod` build tag is
	// used.
	frontend := getFrontendAssets()

	http.Handle("/", http.FileServer(http.FS(frontend)))

	log.Fatalln(http.ListenAndServe(":8080", nil))
}

type Law struct {
	Name       string `json:"name,omitempty"`
	Definition string `json:"definition,omitempty"`
}

var HackerLaws = []Law{
	{
		Name:       "Amdahl's Law",
		Definition: "Amdahl's Law is a formula which shows the potential speedup of a computational task which can be achieved by increasing the resources of a system. Normally used in parallel computing, it can predict the actual benefit of increasing the number of processors, which is limited by the parallelisability of the program.",
	},
	{
		Name:       "Conway's Law",
		Definition: "This law suggests that the technical boundaries of a system will reflect the structure of the organisation. It is commonly referred to when looking at organisation improvements, Conway's Law suggests that if an organisation is structured into many small, disconnected units, the software it produces will be. If an organisation is built more around 'verticals' which are orientated around features or services, the software systems will also reflect this.",
	},
	{
		Name:       "Gall's Law",
		Definition: "A complex system that works is invariably found to have evolved from a simple system that worked. A complex system designed from scratch never works and cannot be patched up to make it work. You have to start over with a working simple system.",
	},
}

func getRandomLaw(w http.ResponseWriter, r *http.Request) {
	randomLaw := HackerLaws[rand.Intn(len(HackerLaws))]
	j, err := json.Marshal(randomLaw)
	if err != nil {
		http.Error(w, "couldn't retrieve random hacker law", http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	io.Copy(w, bytes.NewReader(j))
}
